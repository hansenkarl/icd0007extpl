<?php

require_once 'lib/tpl.php';

$cmd = param('cmd') ? param('cmd') : 'ctf_form';

$data = [];

if ($cmd === 'ctf_form') {
    $data['$title'] = 'Celsius to Fahrenheit';
    $data['$template'] = 'templates/ex3_form.html';
    $data['$cmd'] = 'ctf_calculate';
} else if ($cmd === 'ftc_form') {
    // show form with different title
    // and different command in hidden field
} else if ($cmd === 'ftc_calculate') {
    $input = intval(param('temperature'));
    // calculate result from input (fahrenheit_to_celsius($input))
    // and show result template (templates/ex3_result.html)
} else if ($cmd === 'ctf_calculate') {
    // ...
} else {
    throw new Error('programming error');
}

print render_template('templates/ex3_main.html', $data);

function fahrenheit_to_celsius($temp) {
    return round(($temp - 32) / (9 / 5), 2);
}

function celsius_to_fahrenheit($temp) {
    return round($temp * 9 / 5 + 32, 2);
}

function param($key) {
    if (isset($_GET[$key])) {
        return $_GET[$key];
    } else if (isset($_POST[$key])) {
        return $_POST[$key];
    } else {
        return '';
    }
}

function dump_parameters() {
    print '<pre>';
    print 'GET: ' . print_r($_GET, true);
    print 'POST: ' . print_r($_POST, true);
    print '</pre>';
}
